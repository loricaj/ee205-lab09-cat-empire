///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file test.cpp
/// @version 1.0
///
/// Testing function
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   @todo 4/20/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "cat.hpp"

using namespace std;

const int NUMBER_OF_CATS = 5;

int main() {
	cout << "Welcome to Cat Empire!" << endl;

	Cat::initNames();

	CatEmpire catEmpire;

	for( int i = 0 ; i < NUMBER_OF_CATS ; i++ ) {
		Cat* newCat = Cat::makeCat();

		catEmpire.addCat( newCat );
	}

	cout << "Print a family tree of " << NUMBER_OF_CATS << " cats" << endl;

	catEmpire.catFamilyTree();

} // main()
